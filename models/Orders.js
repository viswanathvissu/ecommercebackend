const { json } = require('express/lib/response')
const mongoose = require('mongoose')
const connectDb = require('../config/db')
const autoIncrement = require('mongoose-auto-increment')

//mongodb+srv://admin:admin123@cluster0.1pw65mz.mongodb.net/?retryWrites=true&w=majority

var connection = mongoose.createConnection("mongodb+srv://admin:admin123@cluster0.1pw65mz.mongodb.net/?retryWrites=true&w=majority");
autoIncrement.initialize(connection);

const OrdersSchema = new mongoose.Schema({
    customer: {
        type: mongoose.Types.ObjectId,
        ref: 'customers',
        required: true
    },
    products: [{
       productId: {
       type: mongoose.Types.ObjectId,
       ref:'product',
       required: true 
       },
       message: {
        type: String,
       },
       status: {
        type: String,
       },
       quantity: {
        type:Number
       },
       productPrice: {
        type: Number
       }
    }],
    deliveryCharges: {
        type: Number,
    },
    gst: {
        type: Number
    },
    deliveryDate: {
        type: Date
    },
    clothCutDate: {
        type: Date
    },
    status : {
        type: String
    },
    paymentMode: {
        type: String
    },
    advancePayment: {
        type: Number
    },
    remainingPayment: {
        type: Number
    },
    totalPrice: {
        type: Number
    },
    remainingPayment: {
        type: Number
    },
    advancePayment:{
        type: Number
    },
    date: {
        type: Date,
        default:Date.now
    }
})

OrdersSchema.plugin(autoIncrement.plugin, {
    model: 'order',
    field: 'orderId',
    startAt: 1100000
})

module.exports= Customer = mongoose.model('order',OrdersSchema)
