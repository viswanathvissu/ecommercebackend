const { json } = require('express/lib/response')
const mongoose = require('mongoose')
const autoIncrement = require('mongoose-auto-increment')

var connection = mongoose.createConnection("mongodb+srv://admin:admin123@cluster0.1pw65mz.mongodb.net/?retryWrites=true&w=majority");
autoIncrement.initialize(connection);


const UsersSchema = new mongoose.Schema({
    username:{
        type: String,
        required: true
    },
    password:{
        type: String,
        required: true
    },
    date:{
        type:Date,
        default:Date.now
    }
})

UsersSchema.plugin(autoIncrement.plugin, {
    model: 'users',
    field: 'userId',
    startAt: 100,
    incrementBy: 100
})

module.exports= Users = mongoose.model('users',UsersSchema)
