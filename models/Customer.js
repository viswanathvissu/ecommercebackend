const { json } = require('express/lib/response')
const mongoose = require('mongoose')
const autoIncrement = require('mongoose-auto-increment')

var connection = mongoose.createConnection("mongodb+srv://admin:admin123@cluster0.1pw65mz.mongodb.net/?retryWrites=true&w=majority");
autoIncrement.initialize(connection);


const CustomerSchema = new mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    address: {
        type:String,
        require: true
    },
    phoneNumber: {
        type: String,
        require: true,
        unique: true
    },
    email:{
        type: String,
        required: false
    },
    date:{
        type:Date,
        default:Date.now
    }

})

CustomerSchema.plugin(autoIncrement.plugin, {
    model: 'customers',
    field: 'customerId',
    startAt: 1002000000,
    incrementBy: 111
})

module.exports= Customer = mongoose.model('customers',CustomerSchema)
