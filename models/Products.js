const { json } = require('express/lib/response')
const mongoose = require('mongoose')
const autoIncrement = require('mongoose-auto-increment')

var connection = mongoose.createConnection("mongodb+srv://admin:admin123@cluster0.1pw65mz.mongodb.net/?retryWrites=true&w=majority");
autoIncrement.initialize(connection);

const ProductsSchema = new mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    actualPrice: {
        type:String,
        require: false
    },
    currentPrice: {
        type: String,
        require: true,
    },
    productDescription: {
        type: String,
        required: true
    },
    images:[{
        type: String
    }],
    tags:[{
        type:String
    }]

})

ProductsSchema.plugin(autoIncrement.plugin, {
    model: 'product',
    field: 'productId',
    startAt: 10008
})

module.exports= Products = mongoose.model('product',ProductsSchema)
