const mongoose = require('mongoose')
const config = require('config')
const db = config.get('mongoURI')

const connectDB = async() => {
    try {
        await mongoose.connect("mongodb+srv://admin:admin123@cluster0.1pw65mz.mongodb.net/?retryWrites=true&w=majority",{
            useNewUrlParser: true, 
            useUnifiedTopology: true 
        })
        console.log("MongoDb connected....")
    }
    catch(err){
        console.log(err.message)
        process.exit(1)
    }
}

module.exports = connectDB