const express = require('express')
const router = express.Router()
const Products = require('../../models/Products')
const config = require('config')

router.post('/addProduct', async (req,res) => {
    try
    {
        console.log("hii")
        const name = req.body.name
        const actualPrice = req.body.actualPrice
        const currentPrice = req.body.currentPrice
        const images = req.body.images
        const productDescription = req.body.productDescription
        const tags = req.body.tags

        console.log(name,actualPrice,currentPrice,images,productDescription,tags)


        const productobj = new Products ({
            name: name,
            actualPrice: actualPrice,
            currentPrice: currentPrice,
            images: images,
            productDescription: productDescription,
            tags: tags
        })

        const productResponse = await productobj.save()
        console.log(JSON.stringify(productResponse))
        res.status(200).json({response: productResponse})

    }
    catch (e) {
        console.log(e)
        res.status(500).send(e)
    }
})

router.get('/listProducts', async (req,res) => {
    try
    {
        var where = {}
        if (req.query.search && req.query.search != ""){
            where.name = req.body.search
        }
        const Page_size = 8;
        const page = parseInt(req.query.page - 1 || "0")
        //console.log("Heyy in pagination",page)
        const products = await Products.find(where).limit(Page_size).skip(Page_size * page)
        const total = await Products.countDocuments({})
        res.status(200).json({response: products,pages: Math.ceil(total/Page_size)})
        return
    }
    catch (e){
        res.status(500).send(e)
    }
})

router.post('/getProduct', async (req,res) => {
    try {
        console.log("I am here")
        const id = req.body.productId
        console.log(id)
        const product = await Products.findOne({id : id})
        if (product){
            res.status(200).json({response: product})
            return
        }
        res.status(200).json({response:"No customer found"})
        return
    }
    catch(e){
        res.status(500).send(e);
    }
})


module.exports = router