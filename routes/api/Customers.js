const express = require('express')
const router = express.Router()
const Customer = require('../../models/Customer')
const config = require('config')

router.post('/addCustomer', async (req,res) => {
    try
    {
        const name = req.body.name
        const phoneNumber = req.body.phoneNumber
        const email = req.body.email
        const address = req.body.address


        const customerobj = new Customer ({
            name: name,
            phoneNumber: phoneNumber,
            address: address,
            email: email ? email : undefined
        })

        const customerResponse = await customerobj.save()
        console.log(JSON.stringify(customerResponse))
        res.status(200).json({response: customerResponse})

    }
    catch (e) {
        res.status(500).send(e)
    }
})

router.get('/listAll', async (req,res) => {
    try
    {
        const customers = await Customer.find({})
        res.status(200).json({response: customers})
        return
    }
    catch (e){
        res.status(500).send(e)
    }
})

router.post('/getCustomer', async (req,res) => {
    try {
        console.log("Yesss")
        console.log(req.body.phoneNumber)
        const phoneNumber = req.body.phoneNumber
        const id = req.body.id
        const customer = await Customer.findOne(id ? {_id : id} : {phoneNumber : phoneNumber})
        if (customer){
            res.status(200).json({response: customer})
            return
        }
        res.status(200).json({response:"No customer found"})
        return
    }
    catch(e){
        res.status(500).send(e);
    }
})

router.post('/deleteCustomer', async (req,res) => {

    try {
        const phoneNumber = req.body.phoneNumber
        const response = await Customer.findOneAndRemove({phoneNumber: phoneNumber})
        const msg = response ?  "Customer removed" : "Customer not found"
        res.status(200).json({response: response, "msg" : msg})
        return
    }
    catch (e){
        res.status(500).send(e)
    }


})

router.post('/editCustomer', async (req,res) => {
    try
    {
        const name = req.body.name
        const phoneNumber = req.body.phoneNumber
        const email = req.body.email
        const address = req.body.address
        const customerId = req.body.customerId


        // const customerobj = new Customer ({
        //     name: name,
        //     phoneNumber: phoneNumber,
        //     address: address,
        //     email: email ? email : undefined
        // })

        // const customerResponse = await customerobj.save()
        const response = await Customer.updateOne({customerId:customerId}, {name:name,phoneNumber:phoneNumber,address:address,email:email})
        console.log(JSON.stringify(response))
        res.status(200).json({response: response})

    }
    catch (e) {
        res.status(500).send(e)
    }
})

module.exports = router