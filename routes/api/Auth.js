const express = require('express')
const router = express.Router()
const Users = require('../../models/Users')
const config = require('config')

router.post('/login', async (req,res) => {
    try
    {
        const email = req.body.email
        const password = req.body.password
        const usersResponse = await Users.findOne({email:email,password:password})
        console.log(usersResponse)
        res.status(200).json({response: usersResponse})

    }
    catch (e) {
        res.status(500).send(e)
    }
})



module.exports = router