const express = require('express')
const router = express.Router()
var easyinvoice = require('easyinvoice');
const Orders = require('../../models/Orders')

router.post('/getInvoice', async (req,res) => {
    try
    {
        const {row} = req.body 
        const {orderId} = req.body
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        let p = [];
        row.products.map((product)=> {
            let ob = {};
            ob["quantity"] = product.quantity
            ob["description"] = product.productId.name
            ob["tax-rate"] = 18
            ob["price"] = product.productPrice
            p.push(ob)
        })
        let ob = {};
        ob["quantity"] = 1
        ob["description"] = "Delivery Charges"
        ob["tax-rate"] = 0
        ob["price"] = row.deliveryCharges
        p.push(ob)
        let q = [
            {
                "quantity": 2,
                "description": "Product 1",
                "tax-rate": 6,
                "price": 33.87
            },
            {
                "quantity": 4.1,
                "description": "Product 2",
                "tax-rate": 6,
                "price": 12.34
            },
            {
                "quantity": 4.5678,
                "description": "Product 3",
                "tax-rate": 21,
                "price": 6324.453456
            }
        ]
        console.log("p = ",p)
        console.log("q = ",q)
        var data = {
            // Customize enables you to provide your own templates
            // Please review the documentation for instructions and examples
            "customize": {
                //  "template": fs.readFileSync('template.html', 'base64') // Must be base64 encoded html 
            },
            "images": {
                // The logo on top of your invoice
                "logo": "https://i.postimg.cc/bYTqyBr6/logo.png",
                // The invoice background
            },
            // Your own data
            "sender": {
                "company": "Paisley Design Botique",
                "address": "Banglore",
                "zip": "1234",
                "city": "Banglore",
                "country": "India"
                //"custom1": "custom value 1",
                //"custom2": "custom value 2",
                //"custom3": "custom value 3"
            },
            // Your recipient
            "client": {
                "company": row.customer.name,
                "address": row.customer.address,
                "zip":  row.customer.phoneNumber,
                "city": row.customer.email,
                "country": row.customer.customerId
                // "custom1": "custom value 1",
                // "custom2": "custom value 2",
                // "custom3": "custom value 3"
            },
            "information": {
                // Invoice number
                "number": row.orderId,
                // Invoice data
                "date": months[new Date(row.date).getMonth()] + " " + (new Date(row.date).getDay()+1) + "," + new Date(row.date).getFullYear(),
                // Invoice due date
                "due-date": row.deliveryDate && months[new Date(row.deliveryDate).getMonth()] + " " + (new Date(row.deliveryDate).getDay()+1) + "," + new Date(row.deliveryDate).getFullYear(),
            },
            // The products you would like to see on your invoice
            // Total values are being calculated automatically
            "products": p,
            // The message you would like to display on the bottom of your invoice
            "bottom-notice": "Thank you for shopping. Visit again",
            // Settings to customize your invoice
            "settings": {
                "currency": "INR", // See documentation 'Locales and Currency' for more info. Leave empty for no currency.
                // "locale": "nl-NL", // Defaults to en-US, used for number formatting (See documentation 'Locales and Currency')
                "tax-notation": "gst", // Defaults to 'vat'
                // "margin-top": 25, // Defaults to '25'
                // "margin-right": 25, // Defaults to '25'
                // "margin-left": 25, // Defaults to '25'
                // "margin-bottom": 25, // Defaults to '25'
                // "format": "A4", // Defaults to A4, options: A3, A4, A5, Legal, Letter, Tabloid
                // "height": "1000px", // allowed units: mm, cm, in, px
                // "width": "500px", // allowed units: mm, cm, in, px
                // "orientation": "landscape", // portrait or landscape, defaults to portrait
            },
            // Translate your invoice to your preferred language
            "translate": {
                // "invoice": "FACTUUR",  // Default to 'INVOICE'
                // "number": "Nummer", // Defaults to 'Number'
                // "date": "Datum", // Default to 'Date'
                // "due-date": "Verloopdatum", // Defaults to 'Due Date'
                // "subtotal": "Subtotaal", // Defaults to 'Subtotal'
                // "products": "Producten", // Defaults to 'Products'
                // "quantity": "Aantal", // Default to 'Quantity'
                // "price": "Prijs", // Defaults to 'Price'
                // "product-total": "Totaal", // Defaults to 'Total'
                // "total": "Totaal" // Defaults to 'Total'
            },
        };
        console.log(row)
        easyinvoice.createInvoice(data, function (result) {
            //console.log('PDF base64 string: ', result.pdf);
            res.status(200).json({response:result})
        });
        return
    }
    catch (e){
        console.log(e)
        res.status(500).send(e)
    }
})


router.post('/getInvoiceOrder', async(req, res) => {
    try{
        console.log("I am at get invoice order !!")
        const orderId = req.body.orderId
        console.log("Heyy Vissu")
        const orders = await Orders.find({orderId:orderId}).populate('customer').populate('products.productId')
        const row = orders.length > 0 ? orders[0] : null
        if (!row) 
        return
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        let p = [];
        row.products.map((product)=> {
            let ob = {};
            ob["quantity"] = product.quantity
            ob["description"] = product.productId.name
            ob["tax-rate"] = 18
            ob["price"] = product.productPrice
            p.push(ob)
        })
        let ob = {};
        ob["quantity"] = 1
        ob["description"] = "Delivery Charges"
        ob["tax-rate"] = 0
        ob["price"] = row.deliveryCharges
        p.push(ob)
        let q = [
            {
                "quantity": 2,
                "description": "Product 1",
                "tax-rate": 6,
                "price": 33.87
            },
            {
                "quantity": 4.1,
                "description": "Product 2",
                "tax-rate": 6,
                "price": 12.34
            },
            {
                "quantity": 4.5678,
                "description": "Product 3",
                "tax-rate": 21,
                "price": 6324.453456
            }
        ]
        console.log("p = ",p)
        console.log("q = ",q)
        var data = {
            // Customize enables you to provide your own templates
            // Please review the documentation for instructions and examples
            "customize": {
                //  "template": fs.readFileSync('template.html', 'base64') // Must be base64 encoded html 
            },
            "images": {
                // The logo on top of your invoice
                "logo": "https://i.postimg.cc/bYTqyBr6/logo.png",
                // The invoice background
            },
            // Your own data
            "sender": {
                "company": "Paisley Design Botique",
                "address": "Banglore",
                "zip": "1234",
                "city": "Banglore",
                "country": "India"
                //"custom1": "custom value 1",
                //"custom2": "custom value 2",
                //"custom3": "custom value 3"
            },
            // Your recipient
            "client": {
                "company": row.customer.name,
                "address": row.customer.address,
                "zip":  row.customer.phoneNumber,
                "city": row.customer.email,
                "country": row.customer.customerId
                // "custom1": "custom value 1",
                // "custom2": "custom value 2",
                // "custom3": "custom value 3"
            },
            "information": {
                // Invoice number
                "number": row.orderId,
                // Invoice data
                "date": months[new Date(row.date).getMonth()] + " " + (new Date(row.date).getDay()+1) + "," + new Date(row.date).getFullYear(),
                // Invoice due date
                "due-date": row.deliveryDate && months[new Date(row.deliveryDate).getMonth()] + " " + (new Date(row.deliveryDate).getDay()+1) + "," + new Date(row.deliveryDate).getFullYear(),
            },
            // The products you would like to see on your invoice
            // Total values are being calculated automatically
            "products": p,
            // The message you would like to display on the bottom of your invoice
            "bottom-notice": "Thank you for shopping. Visit again",
            // Settings to customize your invoice
            "settings": {
                "currency": "INR", // See documentation 'Locales and Currency' for more info. Leave empty for no currency.
                // "locale": "nl-NL", // Defaults to en-US, used for number formatting (See documentation 'Locales and Currency')
                "tax-notation": "gst", // Defaults to 'vat'
                // "margin-top": 25, // Defaults to '25'
                // "margin-right": 25, // Defaults to '25'
                // "margin-left": 25, // Defaults to '25'
                // "margin-bottom": 25, // Defaults to '25'
                // "format": "A4", // Defaults to A4, options: A3, A4, A5, Legal, Letter, Tabloid
                // "height": "1000px", // allowed units: mm, cm, in, px
                // "width": "500px", // allowed units: mm, cm, in, px
                // "orientation": "landscape", // portrait or landscape, defaults to portrait
            },
            // Translate your invoice to your preferred language
            "translate": {
                // "invoice": "FACTUUR",  // Default to 'INVOICE'
                // "number": "Nummer", // Defaults to 'Number'
                // "date": "Datum", // Default to 'Date'
                // "due-date": "Verloopdatum", // Defaults to 'Due Date'
                // "subtotal": "Subtotaal", // Defaults to 'Subtotal'
                // "products": "Producten", // Defaults to 'Products'
                // "quantity": "Aantal", // Default to 'Quantity'
                // "price": "Prijs", // Defaults to 'Price'
                // "product-total": "Totaal", // Defaults to 'Total'
                // "total": "Totaal" // Defaults to 'Total'
            },
        };
        console.log(row)
        easyinvoice.createInvoice(data, function (result) {
            //console.log('PDF base64 string: ', result.pdf);
            res.status(200).json({response:result})
        });
        return
    }
    catch (e){
        console.log(e)
        res.status(500).send(e)
        return 
    }
})

module.exports = router
