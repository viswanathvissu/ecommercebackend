const express = require('express')
const router = express.Router()
const Orders = require('../../models/Orders')
const config = require('config')
const autoIncrement = require('mongoose-auto-increment')
const Customer = require('../../models/Customer')
const notify = require('./Notifications')
const sendMail = require('./Nodemailer')
const Products = require('../../models/Products')

router.post('/createOrder', async (req,res) => {
    try
    {
        console.log("Heyy hiii")
        const cart = req.body.cart
        const customer = req.body.data
        let customerId = customer._id
        const {deliveryCharges,gst,deliveryDate,totalPrice,clothCutDate,remainingPayment,advancePayment} = req.body 
        let items = []
        cart.map((item) => {
            let productId = item.product._id
            items.push({
                productId,
                message:item.product.message || "No customization",
                quantity: item.quantity || 1,
                productPrice: item.product.currentPrice,
            })
        })
        let obj = new Orders({
            customer: customerId,
            products: items,
            deliveryCharges: deliveryCharges,
            gst,
            deliveryDate,
            advancePayment,
            clothCutDate,
            remainingPayment,
            totalPrice
        })


        //console.log(cart,customer)
        // const customerNumber = req.body.customerNumber
        // const item = req.body.phoneNumber
        // const quantity = req.body.email
        // const price = req.body.address
        // const overallPrice = req.body.overallPrice
        // const status = req.body.status
        // const comments = req.body.comments
        // const date = req.body.date

        // const customerObj = await Post.findOne({phoneNumber:customerNumber})
        // const customerObjId = customerObjId._id
        // const ordersObj = new Orders({
        //     customer: customerObjId,
        //     item: item,
        //     quantity: quantity,
        //     price: price,
        //     overallPrice: overallPrice,
        //     deliveryDate: deliveryDate,
        //     status: status,
        //     comments: comments,
        //     date: date
        // })
        //console.log(obj)
        const ordersResponse = await obj.save()
        const customerDetails = await Customer.find({_id: customerId})
        console.log("customer Details",customerDetails[0])
        msg = "Your Order of "
        let con = false
        for (var i =0;i<items.length;i++){
            const item = items[i]
            console.log("item is ",item)
            const pid = item.productId
            const resp = await Products.find({_id:pid})
            console.log("resp is ", resp)
            console.log("product name is ",resp[0].name)
            msg = msg+ resp[0].name +","
            console.log("msg at stpe ",msg)
            if (i == 0){
                con = true
            }
        }
        msg = msg +" was created"
        msg = msg + ` Total Price is ${totalPrice}`
        console.log("msg = ",msg)
        notify(customerDetails[0].phoneNumber,msg)
        sendMail(customerDetails[0].email,msg)
        res.status(200).json({response: ordersResponse})
            return 

    }
    catch (e) {
        console.log(e)
        res.status(500).send(e)
    }
})

router.post('/getOrder', async (req,res) => {
    try
    {
        const orderId = req.body.orderId
        console.log("Heyy Vissu")
        const orders = await Orders.find({orderId:orderId}).populate('customer').populate('products.productId')
        res.status(200).json({response: orders})
        return
    }
    catch (e){
        console.log(e)
        res.status(500).send(e)
    }
})

router.get('/listAll', async (req,res) => {
    try
    {
        console.log(`heyy vissu babu `+ req.query.page)
        const Page_size = 8;
        const page = parseInt(req.query.page || "1")
        //console.log("Heyy in pagination",page)
        //const products = await Products.find({}).limit(Page_size).skip(Page_size * page)
        console.log("Heyy Vissu")
        const orders = await Orders.find({}).limit(Page_size).skip(Page_size * (page -1)).populate('customer').populate('products.productId')
        const total = await Orders.countDocuments({})
        res.status(200).json({response: orders, pages: Math.ceil(total/Page_size)})
        return
    }
    catch (e){
        console.log(e)
        res.status(500).send(e)
    }
})


router.post('/updateStatusOrder', async (req,res) => {
    try
    {
        console.log("Reyy vachamu")
        const {orderId, status} = req.body
        console.log(status)
        const orders = await Orders.find({orderId: orderId}).populate('customer').populate('products.productId')
        console.log("orders is ",orders)
        let updatedOrder = orders[0]
        console.log("updatedOrder = ",updatedOrder)
        updatedOrder.products.map((product) => {
            console.log("product is ",product)
            product.status = status
        })
        updatedOrder.status = status

        console.log("updated Order is ",updatedOrder)
        console.log("products is ",orders[0].products)
        console.log("updatedProducts ",updatedOrder.products)

        const response = await Orders.updateOne({orderId:orderId},{status: status, products: updatedOrder.products})

        res.status(200).json({response: response})
        return
    }
    catch (e){
        console.log(e)
        res.status(500).send(e)
    }
})

router.post('/cancelOrder', async (req,res) => {
    try
    {
        console.log("Reyy vachamu")
        const {orderId} = req.body
        const response = await Orders.deleteOne({orderId: orderId})
        res.status(200).json({response: response})
        return
    }
    catch (e){
        console.log(e)
        res.status(500).send(e)
    }
})


router.post('/updateStatus', async (req,res) => {
    try
    {
        console.log("Reyy vachamu")
        const {orderId, productId, status} = req.body
        const orders = await Orders.find({orderId: orderId}).populate('customer').populate('products.productId')
        console.log("orders is ",orders)
        let updatedOrder = orders[0]
        updatedOrder.products.map((product) => {
            console.log("product is ",product)
            if (product.productId.productId == productId){
                product.status = status
            }
        })

        console.log("updated Order is ",updatedOrder)


        console.log("products is ",orders[0].products)
        console.log("updatedProducts ",updatedOrder.products)

        const response = await Orders.updateOne({orderId:orderId},{products: updatedOrder.products})

        res.status(200).json({response: response})
        return
    }
    catch (e){
        console.log(e)
        res.status(500).send(e)
    }
})

async function updateStatusOP(orderId, productId, status) {
    try
    {
        console.log("Reyy vachamu")
        const orders = await Orders.find({orderId: orderId}).populate('customer').populate('products.productId')
        console.log("orders is ",orders)
        let updatedOrder = orders[0]
        if (productId){
        updatedOrder.products.map((product) => {
            console.log("product is ",product)
            if (product.productId.productId == productId){
                product.status = status
            }
        })
        }
    else {
        updatedOrder.products.map((product) => {
            console.log("product is ",product)
            product.status = status
        })
    }

        console.log("updated Order is ",updatedOrder)


        console.log("products is ",orders[0].products)
        console.log("updatedProducts ",updatedOrder.products)

        const response = await Orders.updateOne({orderId:orderId},{products: updatedOrder.products})
    }
    catch (e){
        console.log(e)
    }
}


router.post('/updateStatusAll', async (req,res) => {
    try{
        console.log("We are in update status all")
        const {selectedOrders, status} = req.body
        console.log('selectedOrders ',selectedOrders)
        console.log('status ',status)
        upOrders = selectedOrders.current 
        const finalResponse = []
        for (var i=0;i<upOrders.length; i++){
            let eachOrder = upOrders[i]
            let orderId = eachOrder.orderId
            let productId = eachOrder.productId
            let response = await updateStatusOP(orderId,productId,status)
            finalResponse.push(response)
        }
        res.status(200).json({response:finalResponse})
        return
    }
    catch (e){
        console.log(e)
        res.status(500).json({message:'error'})
        return 
    }
})

router.post('/updatePrice', async (req,res) => {
    try
    {
        const {orderId,productId,newPrice} = req.body 

        console.log(orderId, productId, newPrice)

        const orders = await Orders.find({orderId: orderId}).populate('customer').populate('products.productId')

        console.log("Heyy babuuuuu")

        let products = orders[0].products
        
        console.log(orders)
        console.log(orders.products)

        products = products.map ((product) => {

            if (product.productId.productId == productId){
                product.productPrice = newPrice
            }

            return product

        })

        console.log("Heyy vissu")

        orders.products = products

        const response = await Orders.updateOne({orderId:orderId},{products: orders.products})

        res.status(200).json({response: response})
            return 

    }
    catch (e) {
        console.log(e)
        res.status(500).send(e)
    }
})

router.post('/updateOrder', async (req,res) => {
    try
    {
        const {orderId,products, status} = req.body 

        //console.log(orderId, productId, newPrice)

        const response = await Orders.updateOne({orderId:orderId},{products:products}, {status:status})

        res.status(200).json({response: response})
            return 

    }
    catch (e) {
        console.log(e)
        res.status(500).send(e)
    }
})

module.exports = router