const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const connectDb = require('./config/db')

connectDb()
const PORT = process.env.PORT || 8000

const app = express()
app.use(cors({
    origin:"*"
}))
app.use(express.json({
    extended:false
}))

//Define Routes 
app.post('/',(req,res) => {
    res.send("Hello")
})

app.use('/api/customer',require('./routes/api/Customers'))
app.use('/api/notication',require('./routes/api/Notifications'))
app.use('/api/products',require('./routes/api/Products'))
app.use('/api/orders',require('./routes/api/Orders'))
app.use('/api/invoice',require('./routes/api/Invoice'))
app.use('/api/auth',require('./routes/api/Auth'));

app.listen(PORT,()=> console.log('Hello Server'))